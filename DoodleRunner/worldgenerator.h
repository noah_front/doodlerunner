/* INCLUDE GUARDS */
#ifndef WORLDGENERATOR_H
#define WORLDGENERATOR_H

#include "raylib.h"

#define STARTING_PLATFORM       1
#define DEFAULT_PLATFORM_SIZE   GetScreenWidth() / (13*2)
#define DEFAULT_PLATFORM_HEIGHT GetScreenHeight() / (49 * 2)
#define MAX_PLATFORMS_PER_LINE  26
#define MAX_PLATFORMS           20

extern Vector2 platformSize;

typedef enum Direction {

	RIGHT, LEFT
} Direction;

typedef struct Platform {

	Vector2 position;
	Vector2 movPos;

	Rectangle rectangle;

	Color color;
	bool destroyed;
	bool moveable;
	Direction direction;
	
} Platform;

/* Prototypes for the functions */
unsigned int getByChance(int chance);

/* Returns a random number between two numbers. */
unsigned int getBetweenInteval(unsigned int min, unsigned int max);

/* Generates a world (platforms) by a percentage (e.g: 20 = 20%) and returns how many platforms were moved */
void generateWorld(int chance, Platform[MAX_PLATFORMS][MAX_PLATFORMS_PER_LINE]);

/* Regenerates an existing world (platforms) by a percentage (e.g: 20 = 20%) and returns how many platforms were moved */
int regenerateWorld(int chance, int gameMovement, Platform[MAX_PLATFORMS][MAX_PLATFORMS_PER_LINE]);

#endif