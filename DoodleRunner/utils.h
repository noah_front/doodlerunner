#ifndef UTILS_H
#define UTILS_H

#include "raylib.h"
#include "worldgenerator.h"


/* Returns true when col1 and col2 are the same */
bool sameColor(Color col1, Color col2);

/* Checks if a certain platform-line is currently viewable */
bool isViewable(int i, int j, Platform platforms[MAX_PLATFORMS][MAX_PLATFORMS_PER_LINE]);

#endif