#include "gamescreen.h"

//Variablennnnn
//--------------------------------------------------------------------------------------

Rectangle rectangleButtons[2];					 // Rectangles array
bool      selectedButtons[2] = { false };        // Selected rectangles indicator
Vector2   mousePoint;
Texture2D background;
Texture2D logo;
Texture2D logo2;
Texture2D logo3;

//-------------------------------------------------------------------------------------
//StartScreen

void StartScreen(void) {

	// Initialization
	//--------------------------------------------------------------------------------------

	background = LoadTexture("images/bg-grid.png");          // Load image in CPU memory (RAM)  |  Ratio 12 zu 13| 600 x 650
	background.width = GetScreenWidth();
	background.height = background.width * 0.92307692307692307692307f;

	logo = LoadTexture("images/doodlelogo.png");	   		 // Load image in CPU memory (RAM)  |  Ratio 182 zu 103 | 1638 x 927
	logo.height = GetScreenHeight() / 3;
	logo.width = logo.height * 1.76699029126213f;

	logo2 = LoadTexture("images/spielen.png");      // Load image in CPU memory (RAM)  |  Ratio 182 zu 103 | 1580 x 683
	logo2.height = GetScreenHeight() / 10;
	logo2.width = logo2.height * 2.3133235724743777f;

	//logo3 = LoadTexture("images/resolution.png");
	//logo3.height = logo2.height * 0.9f;
	//logo3.width = logo3.height * 4.386363636363636363636363f;

	rectangleButtons[0].width = logo2.width;
	rectangleButtons[0].height = logo2.height;
	rectangleButtons[0].x = GetScreenWidth() / 2 - logo2.width / 2;
	rectangleButtons[0].y = GetScreenHeight() / 2;

	//rectangleButtons[1].width = logo3.width;
	//rectangleButtons[1].height = logo3.height;
	//rectangleButtons[1].x = GetScreenWidth() / 2 - logo3.width / 2;
	//rectangleButtons[1].y = GetScreenHeight() / 2 + logo2.height * 2;

	//--------------------------------------------------------------------------------------



	//--------------------------------------------------------------------------------------
	// StartScreenLoop
	while (!selectedButtons[0] && !WindowShouldClose())    // Detect window close button or ESC key
	{

		// Update
		//----------------------------------------------------------------------------------
		mousePoint = GetMousePosition();

		for (int i = 0; i < 2; i++)    // Iterate along all the rectangles
		{	
			if (CheckCollisionPointRec(mousePoint, rectangleButtons[i]) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			{
				selectedButtons[i] = !selectedButtons[i];
			}
		}
		//----------------------------------------------------------------------------------

		// Draw
		//----------------------------------------------------------------------------------
		BeginDrawing();
		//Hintergrund
		ClearBackground(RAYWHITE);

		DrawTexture(background, 0, 0, WHITE);

		for (int i = 0; i < 2; i++)    // Draw all rectangles
		{
			DrawRectangleRec(rectangleButtons[i], BLANK);
		}
		//Vordergrund
		DrawTexture(logo, GetScreenWidth() / 2 - logo.width / 2, GetScreenHeight() / 16, WHITE);
		DrawTexture(logo2, GetScreenWidth() / 2 - logo2.width / 2, GetScreenHeight() / 2, WHITE);
	//	DrawTexture(logo3, GetScreenWidth() / 2 - logo3.width / 2, GetScreenHeight() / 2 + logo2.height * 2, WHITE);

		//--------------------------------------------------------------------------------------
		EndDrawing();
		//----------------------------------------------------------------------------------

	}
}
