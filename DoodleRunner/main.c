// TUHH - Prozedurale Programmierunng - Finales Projekt
// Gruppe 127; Studenten: Marvin Heydorn, David Zeplin, Noah Frontzkowski.
// DoodleRunner - eine Mischug aus Doodle Jump und Line Runner.


//----------------------------------------------------------------------------------
// Include "raylib" - Gamelibrary
#include "raylib.h"

#include "worldgenerator.h"
#include "gamescreen.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
//----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------
// Some Defines!
#define PLAYER_MAX_LIFE         1
#define GRAVITY                 9.81f
#define DEFAULT_FPS             60
#define PLATFORM_MAX_MOVEMENT   200
#define PLATFORM_SPEED          5
//----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------
// Types and Structures Definition
typedef enum GameState { LOGO, TITLE, GAMEPLAY, HIGHSCORE, ENDING } GameState;

typedef struct Player {
	Vector2 position;
	Vector2 speed;
	int radius;
	int life;
} Player;

//----------------------------------------------------------------------------------


//----------------------------------------------------------------------------------
// Global Variables Declaration

// INTEGER VARIABLEN
static int screenWidth = 1366;
static int screenHeight = 768;
static int gameMovement = 4;

// SCORE VARIABLEN
FILE *highscore;

static int score = 0;
static int hiScore = 0;

// RAYLIB EIGENE VARIABLEN
static Player player;

// Default platforms
Platform platforms[MAX_PLATFORMS][MAX_PLATFORMS];

Texture2D GameOverImage;
Texture2D GrünePlatform;

// BOOLS
static bool gameOver;
static bool isRunning = false;
static bool pause;
//----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------
// Module Functions Declaration (local)
static void InitGame(void);                                           // Initialize game
static void UpdateGame(void);                                         // Update game (one frame)
static void DrawGame(void);                                           // Draw game (one frame)
static void UnloadGame(void);                                         // Unload game
static void UpdateDrawFrame(void);                                    // Update and Draw (one frame)                        
//----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------|
// Program main entry point                                                            |
//----------------------------------------------------------------------------------|


int main(void) {

	//---------------------------------------------------------
	SetConfigFlags(FLAG_VSYNC_HINT);
	//SetConfigFlags(FLAG_FULLSCREEN_MODE);
	SetTargetFPS(DEFAULT_FPS);

	InitWindow(screenWidth, screenHeight, "DoodleRunner");

	StartScreen();
	if (!selectedButtons[1])
	{
		//ResolutionSetting();
	}
	InitGame();
	//--------------------------------------------------------------------------------------


	// MAIN GAME LOOP
	//--------------------------------------------------------------------------------------
	while (!WindowShouldClose()) {    // DETECT WINDOW CLOSE BUTTON OR ESC
		UpdateDrawFrame();    // Update and Draw
	}
	//--------------------------------------------------------------------------------------

	// CLEAN-UP Logic
	//--------------------------------------------------------------------------------------
	UnloadGame();         // Unload loaded data (textures, sounds, models...)
	CloseWindow();        // Close window and OpenGL context
	//--------------------------------------------------------------------------------------

	return 0;
}

//------------------------------------------------------------------------------------
// Module Functions Definitions (local)
//------------------------------------------------------------------------------------

void ResolutionSetting(void) {

	//TODO

}
// Initialize game variables
void InitGame(void) {

	// Initialize player
	player.position = (Vector2) { GetScreenWidth() / 2, GetScreenHeight() / 2 + 100 };
	player.radius = DEFAULT_PLATFORM_SIZE / 5;
	player.life = PLAYER_MAX_LIFE;
	player.speed.y = GRAVITY;

	/* Intializes random number generator */
	srand((unsigned)time(0));
	generateWorld(7, platforms);

	/* SAFE-PLATFORM: Der Spieler muss die Chance haben beim Start eine Plattform zu treffen.
	Diese Plattform ist in jedem Level unten-mittig auf dem Bildschirm zentriert. Sie zerbricht, sobald der Spieler sie einmal

	*/
	platforms[STARTING_PLATFORM][STARTING_PLATFORM].position.x = GetScreenWidth() / 2;
	platforms[STARTING_PLATFORM][STARTING_PLATFORM].position.y = GetScreenHeight() - 50;

	platforms[STARTING_PLATFORM][STARTING_PLATFORM].rectangle.height = DEFAULT_PLATFORM_HEIGHT;
	platforms[STARTING_PLATFORM][STARTING_PLATFORM].rectangle.width = DEFAULT_PLATFORM_SIZE;
	platforms[STARTING_PLATFORM][STARTING_PLATFORM].rectangle.x = platforms[STARTING_PLATFORM][STARTING_PLATFORM].position.x - DEFAULT_PLATFORM_SIZE / 2;

	platforms[STARTING_PLATFORM][STARTING_PLATFORM].rectangle.y = platforms[STARTING_PLATFORM][STARTING_PLATFORM].position.y - DEFAULT_PLATFORM_HEIGHT / 2;
	platforms[STARTING_PLATFORM][STARTING_PLATFORM].destroyed = false;
	platforms[STARTING_PLATFORM][STARTING_PLATFORM].color = ORANGE;

	// TODOOOOOOOOOOOOOOOOOOO

}


bool up = true;
int x = 0, y = 0;
// UPDATE GAME (ONE-FRAME)
void UpdateGame(void)
{
	// Entfernt den curser wenn man gerade am spielen ist
	if (!gameOver && !pause) HideCursor();
	else ShowCursor();

	if (!gameOver)
	{
		//--------------------------------------------------------------------------------------
		// Start logic
		if (!isRunning) {
			if (IsKeyPressed(KEY_SPACE)) {
				isRunning = true;
			}
			return;
		}
		//--------------------------------------------------------------------------------------

		// PAUSE LOGIC
		if (IsKeyPressed('P')) pause = !pause;

		if (!pause) {

			//--------------------------------------------------------------------------------------
			// PLAYER MOVEMENT LOGIC
			if (IsKeyDown(KEY_LEFT)) player.position.x -= 6;

			// MOVE TO THE LEFT
			if ((player.position.x - player.radius / 2) <= 0) player.position.x = player.radius / 2;
			if (IsKeyDown(KEY_RIGHT)) player.position.x += 6;

			// MOVE TO THE RIGHT
			if ((player.position.x + player.radius / 2) >= GetScreenWidth()) player.position.x = GetScreenWidth() - player.radius / 2;
			//--------------------------------------------------------------------------------------


			//--------------------------------------------------------------------------------------
			// COLLISION LOGIC && PLATFORM MOVEMENT LOGIVC
			if (player.life >= 1) {
				for (int i = 0; i < MAX_PLATFORMS; i++) {
					for (int j = 0; j < MAX_PLATFORMS_PER_LINE; j++) {

						if (platforms[i][j].destroyed) {
							continue;
						}

						// Collision logic: Player vs Platform (Wird nur getestet, wenn der Spiele sich nicht bereits aufwärts bewegt.. (fliegt sonst durch die Platformen)
						if (player.speed.y >= 0 &&
							CheckCollisionCircleRec(player.position, player.radius, platforms[i][j].rectangle)) {

							Color col = platforms[i][j].color;

							if (sameColor(col, ORANGE)) {
								platforms[i][j].color = RED;
							}
							else if (sameColor(col, RED)) {
								platforms[i][j].destroyed = true;
							}

							// ADJUST PLAYER SPEED TO THE SCREEN MOVING RATE 
							if ((player.position.y - player.radius) < GetScreenHeight() / 2) {
								player.speed.y = -GRAVITY + gameMovement;

							}
							else {

								player.speed.y = -GRAVITY;
							}
						}

						// PLATFORM MOVEMENT LOGIC
						if (platforms[i][j].moveable) {

							int xDiff = abs(platforms[i][j].rectangle.x - platforms[i][j].movPos.x);

							if (xDiff >= PLATFORM_MAX_MOVEMENT) {

								if (platforms[i][j].direction == LEFT) {
									platforms[i][j].direction = RIGHT;
									platforms[i][j].rectangle.x -= PLATFORM_SPEED;
								}
								else {
									platforms[i][j].direction = LEFT;
									platforms[i][j].rectangle.x += PLATFORM_SPEED;
								}
							}
							else {

								platforms[i][j].rectangle.x += platforms[i][j].direction == RIGHT ? -PLATFORM_SPEED : PLATFORM_SPEED;
							}
						}

					}
				}

			}

			//--------------------------------------------------------------------------------------
			// COLLISION LOGIC: PLAYER VS WALLS
			if (((player.position.x + player.radius) >= GetScreenWidth())) player.position.x = 10;
			else if (((player.position.x - player.radius) <= 0)) player.position.x = GetScreenWidth() - 10;

			//--------------------------------------------------------------------------------------
			// PLAYER MOVEMENT LOGIC (GRAVITY)
			if (player.life >= 1) {
				player.position.y += player.speed.y;
				player.speed.y += GRAVITY / GetFPS();
			}
			else {
				player.position = (Vector2) { player.position.x, GetScreenWidth() * 7 / 8 - 30 };
			}
			//--------------------------------------------------------------------------------------


			//--------------------------------------------------------------------------------------
			// GAME-EXTENSION
			if ((player.position.y - player.radius) < GetScreenHeight() / 2)
			{
				player.speed.y = player.speed.y;

				score += regenerateWorld(10, gameMovement, platforms);
                if (score > hiScore)
                {
                    hiScore = score;
                    
                }
			}

			//Wenn gameOver zutrifft wird der score auf null gesetzt
			if ((player.position.y + player.radius) >= GetScreenHeight()) {
				gameOver = true;
				player.life--;

			}
			//--------------------------------------------------------------------------------------
		}

	}
	else {
        // Highscore wird in txt gespeichert!!
        int globalHiScore;
        highscore = fopen("Highscore.txt", "r");
        fscanf(highscore, "%d", &globalHiScore);
        fprintf(highscore, "%d", globalHiScore);
        
        if(hiScore > globalHiScore){
        globalHiScore = hiScore;
        highscore = fopen("Highscore.txt", "w");
        fscanf(highscore, "%d", &globalHiScore);
        fprintf(highscore, "%d", globalHiScore);
        }
        fclose(highscore);
        //------------------------------------------------------------------------------------------------------------------------//
        // DrawFunktionen für den Gameover Screen
        //Das Logo und der Hintergrund---------------------------------------------------------------------------------------------//
        Image LogoGameOver = LoadImage("images/gameOver.png");     // Load image in CPU memory (RAM)  |  Ratio 182 zu 103 | 1638 x 927
        GameOverImage = LoadTextureFromImage(LogoGameOver);
        UnloadImage(LogoGameOver);
        GameOverImage.height = GetScreenHeight() / 3;
        GameOverImage.width = GameOverImage.height + GameOverImage.height / 2;

        //------------------------------------------------------------------------------------------------------------------------//
        //------------------------------------------------------------------------------------------------------------------------//
        DrawTexture(background, 0, 0, WHITE);
		DrawTexture(GameOverImage, GetScreenWidth() / 2 - GameOverImage.width / 2, GetScreenHeight() / 16, WHITE);
		DrawText("ENTER TO RESTART THE GAME", GetScreenWidth() / 2 - MeasureText("ENTER TO RESTART THE GAME", 40) / 2, GetScreenHeight() / 2 - 40, 40, BLACK);
		DrawText(FormatText("Dein Score: %07i", score), GetScreenWidth() / 2 - 220, GetScreenHeight() / 2 + 30, 40, BLACK);
		DrawText(FormatText("HI-SCORE: %07i", globalHiScore), GetScreenWidth() / 2 - 200, GetScreenHeight() / 2 + 80, 40, BLACK);
	}

	//--------------------------------------------------------------------------------------
	// RESTART GAME
	if (IsKeyPressed(KEY_ENTER)) {
		InitGame();
		score = 0;
		isRunning = false;
		gameOver = false;
	}
	//--------------------------------------------------------------------------------------
}

// Draw game (one frame)
void DrawGame(void) {

	BeginDrawing();

	ClearBackground(RAYWHITE);

	if (!gameOver) {
		// DRAW BACKGROUND
		DrawTexture(background, 0, 0, WHITE);

		// Draw player
		DrawCircle(player.position.x, player.position.y, player.radius, MAROON);

		//--------------------------------------------------------------------------------------
		// DRAW PLATFORMS
		GrünePlatform = LoadTexture("images/Jump_Plattform_Gruen.png");
		
		for (int i = 0; i < MAX_PLATFORMS; i++) {
			for (int j = 0; j < MAX_PLATFORMS_PER_LINE; j++) {
				if (!platforms[i][j].destroyed) {

					GrünePlatform.height = platforms[i][j].rectangle.height;
					GrünePlatform.width = platforms[i][j].rectangle.width;
					DrawTexture(GrünePlatform, platforms[i][j].rectangle.x, platforms[i][j].rectangle.y, WHITE);
			
				}
			}
		}
		//--------------------------------------------------------------------------------------
		// Text auf dem Bildschirm der den score und den HI-Score printet
		DrawText(FormatText("%07i", score), 40, 20, 30, GRAY);

		// HI-SCORE-PAUSE-SCREEN (auch in die gamescreen.c Klasse moven). (TODO)
		if (pause) DrawText("GAME PAUSED", GetScreenWidth() / 2 - MeasureText("GAME PAUSED", 40) / 2, GetScreenHeight() / 2 - 40, 40, GRAY);
	}
	EndDrawing();
}

// UNLOAD ALL GAME VARIABLES
void UnloadGame(void)
{
	UnloadTexture(GrünePlatform);
	// TODO: Unload all dynamic loaded data (textures, sounds, models...)
	/*platform = LoadTextureFromImage(PlatformImage);
	logo.height = GetScreenHeight() / (49 * 2);
	logo.width = GetScreenWidth() / (13 * 2);
	*/
}

// Update and Draw (one frame)
void UpdateDrawFrame(void)
{
	UpdateGame();
	DrawGame();
}

