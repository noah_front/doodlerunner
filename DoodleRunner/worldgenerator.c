/* In general it's good to include also the header of the current .c,
   to avoid repeating the prototypes */
#include "worldgenerator.h"
#include "raylib.h" 

#include <stdio.h>
#include <stdlib.h>

 Vector2 platformSize;

// Returns true/false based on the chance given (e.g. = 20 = 20% = 1/5%)
unsigned int getByChance(int chance) {

	int zufall = rand() % 100;
	return chance <= zufall ? 1 : 0;
}
 
// Returns a random number between two numbers.
unsigned int getBetweenInteval(unsigned int min, unsigned int max) {
	int r;
	const unsigned int range = 1 + max - min;
	const unsigned int buckets = RAND_MAX / range;
	const unsigned int limit = buckets * range;

	/* Create equal size buckets all in a row, then fire randomly towards
	 * the buckets until you land in one of them. All buckets are equally
	 * likely. If you land off the end of the line of buckets, try again. */
	do
	{
		r = rand();
	} while (r >= limit);

	return min + (r / buckets);
}

//bool isReachable(Platform platform1, Platform platform2) {
//
//}


void generateWorld(int chance, Platform platforms[MAX_PLATFORMS][MAX_PLATFORMS_PER_LINE]) {

	// SET DEFAULT PLATFORMSIZE
	platformSize = (Vector2) { GetScreenWidth() / MAX_PLATFORMS_PER_LINE, DEFAULT_PLATFORM_SIZE };

	int initialDownPosition = 50, lastY = -1;
	for (int i = 0; i < MAX_PLATFORMS; i++)
	{
		for (int j = 0; j < MAX_PLATFORMS_PER_LINE; j++)
		{
			platforms[i][j].position = (Vector2) { j*platformSize.x + platformSize.x / 2, i*platformSize.y };
			platforms[i][j].movPos = (Vector2) { j*platformSize.x + platformSize.x / 2, i*platformSize.y };
			platforms[i][j].direction = RIGHT;

			platforms[i][j].moveable = getBetweenInteval(0, 100) <= 5;
			platforms[i][j].destroyed = getByChance(chance);
			platforms[i][j].color = GREEN;

			platforms[i][j].rectangle.height = DEFAULT_PLATFORM_HEIGHT;
			platforms[i][j].rectangle.width = DEFAULT_PLATFORM_SIZE;
			platforms[i][j].rectangle.x = platforms[i][j].position.x - DEFAULT_PLATFORM_SIZE / 2;
			platforms[i][j].rectangle.y = platforms[i][j].position.y - DEFAULT_PLATFORM_HEIGHT / 2;

		}
	}
}

/* Regenerates the world so it seem's like the screen is moving upwards.
Returns how many platforms were moved */
int regenerateWorld(int chance, int gameMovement, Platform platforms[MAX_PLATFORMS][MAX_PLATFORMS_PER_LINE]) {

	int lastPlatformY = -1, score = 0;
	for (int i = 0; i < MAX_PLATFORMS; i++)
	{
		for (int j = 0; j < MAX_PLATFORMS_PER_LINE; j++)

		{
			platforms[i][j].rectangle.y += gameMovement;
			score += 1;

			if (platforms[i][j].rectangle.y > (GetScreenHeight())) {
				platforms[i][j].rectangle.y = 0;

				bool random = getByChance(chance);
				if ((platforms[i][j].rectangle.y - lastPlatformY) >= 5) {
					random = false;
				}

				platforms[i][j].destroyed = random;
				if (!platforms[i][j].destroyed) {
					lastPlatformY = platforms[i][j].rectangle.y;
				}
			}
		}
	}

	return score;
}
