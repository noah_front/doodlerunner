/* INCLUDE GUARDS */
#ifndef STARTSCREEN_H
#define STARTSCREEN_H

#include "raylib.h"

extern Texture2D background;
extern Texture2D logo;
extern bool      selectedButtons[2];        // Selected rectangles indicator
extern Vector2   mousePoint;
void StartScreen(void);

#endif