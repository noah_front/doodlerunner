#include "worldgenerator.h"
#include "raylib.h"

#include <stdio.h>
#include <stdbool.h>

bool sameColor(Color col1, Color col2) {
    return col1.a == col2.a
        && col1.b == col2.b
        && col1.g == col2.g
        && col1.r == col2.r;
}
bool isViewable(int i, int j, Platform platforms[MAX_PLATFORMS][MAX_PLATFORMS_PER_LINE]) {

    for (i; i < MAX_PLATFORMS; i++) {
        for (j; j < MAX_PLATFORMS_PER_LINE; j++) {
            if (platforms[i][j].rectangle.y < GetScreenHeight()) {
                continue;
            }

            return false;
        }
    }

    return true;
}
