# DoodleRunner (Spiel fuer Prozedurale Programmierung)

Inhalt

Spezifikation (A)        1

Design (B)        2



# Spezifikation (A)

Wir programmieren ein Spiel, dass eine Adaption von Doodle Jump1 und Line Runner2 werden soll. Es soll in der fertigen Implementation Elemente aus beiden Spielen vereinen, sodass man sich im fertigen Spiel sowohl horizontal als auch vertikal bewegen muss, um ein Level zu bewerkstelligen.

Im Laufe eines jeden Levels muss der Spieler mehrere horizontale wie vertikale Abschnitte bewältigen. Es wird ein „Score&quot; implementiert, der anzeigt wie viel Blöcke der Spiele sich bereits in die Zielrichtung bewegt hat.

Das Spiel ist vorbei, sobald der Spiele ins „Leere&quot; fällt oder das nächste Level erreicht hat.

Unsere acht (wesentliche) Leistungsmerkmale sind:

1. Level
2. Zähler (Zeit &amp; Score)
3. Horizontales- und vertikales Bewegen
4. Verschiedene Spielelemente (Blöcke)
5. Verschiedene Schwierigkeitsstufen
6. Maus und Tastatureingabe möglich
7. Audio, die sich dem Spielgeschehen anpasst
8. Zufällig generierte Gegner



# Design (B)

1. Programmstruktur/ Aufbau des Programms
2. Wichtigste Funktionen und ihre Signatur (Parameter und Rückgabewert)
3. zentrale (strukturierte) Datentypen

1

#
[https://de.wikipedia.org/wiki/Doodle\_Jump](https://de.wikipedia.org/wiki/Doodle_Jump)

2

#
[https://play.google.com/store/apps/details?id=com.djinnworks.linerunnerfree&amp;hl=de](https://play.google.com/store/apps/details?id=com.djinnworks.linerunnerfree&amp;hl=de)